import { Component, OnInit } from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';



/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface FoodNode {
  name: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'Associate Front End Developer, Betr Consulting LLP(On-site) May 19 - Dec 19',
    children: [
      {
        name: 'Developed website using Angular, its support SCO search Engin optimization'},
        {
          name: 'Configure firebase for storing form data'}
    ]
  },
  {
    name: 'Project Software Engineer, IIT Bombay (On-site) July 17 - Feb 19',
    children: [
      {name: 'Design, development and testing drupal 8 module'},
      {name: 'Change and modified existing drupal module and themes'},
      {name: 'Configured various Drupal modules to achieve the desired functionality.'},
    ]
  }, 
  {
    name: 'Trainee Software Engineer, Mastek PVT LTD (On-site)  July 15 - Dec 15',
    children: [
      {name: 'Developed Investment Details Application using Java, HTML, CSS, javaScript'}
    ]
  }, 
];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

/**
 * @title Tree with flat nodes
 */

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent implements OnInit {
  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
 
  ngOnInit() {
  }

  private _transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
    this.dataSource.data = TREE_DATA;
  }
}
