import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CvComponent } from './cv/cv.component';
import { UsersComponent } from './users/users.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { OurProfileComponent } from './our-profile/our-profile.component';


const routes: Routes = [
  {
  path: 'login-page', component: LoginPageComponent
},
{
  path: 'cv', component: CvComponent
},
{
  path: 'contact-us', component: ContactUsComponent
},
{
  path: '', component: OurProfileComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }