import { Component, OnInit } from '@angular/core';


export interface PeriodicElement {
  name: string;
  position: string;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 'M.Tech, Computer Science Eng.', name: 'IIT Bombay', symbol: 'IIT Bombay', weight: 2015},
  {position: 'B.Tech, Computer Science Eng.', name: 'NIT Jamshedpur', symbol: 'NIT Jamshedpur', weight: 2011},
];

@Component({
  selector: 'app-academic-details-table',
  templateUrl: './academic-details-table.component.html',
  styleUrls: ['./academic-details-table.component.scss']
})
export class AcademicDetailsTableComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'symbol', 'weight'];
  dataSource = ELEMENT_DATA;
  constructor() { }

  ngOnInit() {
  }

}
