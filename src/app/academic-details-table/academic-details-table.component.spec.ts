import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicDetailsTableComponent } from './academic-details-table.component';

describe('AcademicDetailsTableComponent', () => {
  let component: AcademicDetailsTableComponent;
  let fixture: ComponentFixture<AcademicDetailsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcademicDetailsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicDetailsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
